#ifndef _LINESEG_H_
#define _LINESEG_H_

#include "SimpleImage.h"
#include "util.h"

class LineSeg {
	public:
		// Data members - just two points
		Point3f p1, p2;

		// Constructor
		LineSeg(Point3f p1_, Point3f p2_) {p1 = p1_; p2 = p2_;}

		// Detect line segment intersection
		bool isIntersect(LineSeg l2);

	private:
};


#endif
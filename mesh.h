#ifndef _MESH_H_
#define _MESH_H_

#include <vector>
#include <string>

#include "SimpleImage.h"
#include "lineseg.h"
#include "util.h"

struct Vertex {
	Point3f coord;
	RGBColor color;
};

struct Triangle {
	int idx0, idx1, idx2;
	int vdx0, vdx1, vdx2;
	int ndx0, ndx1, ndx2;	

	RGBColor color;

	Triangle() { 
		idx0 = idx1 = idx2 = -1;
		vdx0 = vdx1 = vdx2 = -1;
		ndx0 = ndx1 = ndx2 = -1;
	}	
};

class Mesh {
	public:
    
		// Data members
		std::vector<Vertex> verts;
		std::vector<Triangle> tris;
		std::vector<Point2f> texs;
		std::vector<Point3f> norms;
		std::vector<LineSeg> bound;
		bool has_tex;
		bool has_norm;
		float length, width;

		std::string filename;		

		void initialize(const std::string& filename);
		// For track, read the boundary segments from file
		void initbound(const std::string& filename);
		// For car, simply calculates the boundary segments
		void calcbound();

		// Normalize the geometry
		void normalize();

		// Test functions
		void print_verts(); 
		void print_tris();
		
		// draw triangles in OpenGL, later use frambuffer object
		void draw();

	private:
		// Private helper functions for parsing data
		void process_vertex(std::string& line);
		void process_texture(std::string& line);
		void process_normal(std::string& line);
		void process_face(std::string& line);
		
		// draw with (verts/norms/texs)
		void drawV(void);
		void drawVN(void);
		void drawVT(void);
		void drawVNT(void);

};


#endif

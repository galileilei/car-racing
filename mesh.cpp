#include <GL/glut.h>
#include <fstream>
#include <iostream>
#include <string>
#include <math.h>

#include "mesh.h"

void Mesh::initialize(const std::string& filename) {
	std::ifstream ifs(filename.c_str());
	std::string line;

	// Check whether the file is open 
	if (! ifs.is_open()) {
		std::cerr<<"Cannot open file "<<filename<<std::endl;
		return;
	}

	// Start parsing .obj data
	while (std::getline(ifs, line)) {
		// Skip the empty line
		if (line == "") continue;
		// Skip the commented line
		if (line[0] == '#') continue;

		// Process the three different conditions
		if (line.substr(0, 2) == "v ") {
			// Vertex
			process_vertex(line);
		} else if (line.substr(0, 2) == "vt") {
			// Texture
			process_texture(line);
		} else if (line.substr(0, 2) == "vn") {
			// Normal
			process_normal(line);
		} else if (line.substr(0, 2) == "f ") {
			// Face
			process_face(line);
		} else {
			// Cannot process this type
		}
	}
	
	this->has_tex = this->texs.size() > 0 ? true : false;
	this->has_norm = this->norms.size() > 0 ? true : false;
	// Normalize the geometry, meaning that the maximum coordinate is 1.0 (or -1.0)
	// normalize();

	// Set filename
	this->filename = filename;
	if (this->filename.find('.') != std::string::npos)
		this->filename = this->filename.substr(0, this->filename.find('.')+1);
}

void Mesh::process_vertex(std::string& line) {
	Vertex vtx;
	Point3f p3;

	if (sscanf(line.c_str(),"v %f %f %f", &(p3.x), &(p3.y), &(p3.z)) == EOF) {
		std::cerr<<"Cannot parse this line: "<<line<<std::endl;
		return;
	}		

	// If successfully parsing the vertex data, add it to the vector
	vtx.coord = p3;
	verts.push_back(vtx);
}

void Mesh::process_texture(std::string& line) {
	Point2f p2;

	if (sscanf(line.c_str(),"vt %f %f", &(p2.x), &(p2.y)) == EOF) {
		std::cerr<<"Cannot parse this line: "<<line<<std::endl;
		return;
	}		

	// If successfully parsing the texture data, add it to the vector
	texs.push_back(p2);
}

void Mesh::process_normal(std::string& line) {
	Point3f p3;

	if (sscanf(line.c_str(),"vt %f %f %f", &(p3.x), &(p3.y), &(p3.z)) == EOF) {
		std::cerr<<"Cannot parse this line: "<<line<<std::endl;
		return;
	}		

	// If successfully parsing the normal data, add it to the vector
	norms.push_back(p3);	
}

void Mesh::process_face(std::string& line) {

	/* There exists four different patterns:
		f v1 v2 v3
		f v1/t1 v2/t2 v3/t3
		f v1//n1 v2//n2 v3//n3
		f v1/t1/n1 v2/t2/n2 v3/t3/n3		
	 */

	std::string::size_type idx = line.find('/');
	std::string::size_type idx2= line.find("//");

	int cnt = 0;
	for (unsigned int i = 0; i < line.size(); i++)
		if (line[i] == '/') cnt++;

	Triangle tri;

	// 1. f v1 v2 v3
	if (idx == std::string::npos) {
		if (sscanf(line.c_str(),"f %d %d %d", &(tri.idx0), &(tri.idx1), &(tri.idx2)) == EOF) {
			std::cerr<<"Cannot parse this line: "<<line<<std::endl;
			return;
		}	
	} 
	// 2. f v1/t1 v2/t2 v3/t3
	else if (cnt == 3) {
		if (sscanf(line.c_str(),"f %d/%d %d/%d %d/%d", 
								&(tri.idx0), &(tri.vdx0),  
								&(tri.idx1), &(tri.vdx1),
								&(tri.idx2), &(tri.vdx2)) == EOF) {
			std::cerr<<"Cannot parse this line: "<<line<<std::endl;
			return;
		}
	}
	// 3. f v1//n1 v2//n2 v3//n3
	else if (idx2 != std::string::npos) {
		if (sscanf(line.c_str(),"f %d//%d %d//%d %d//%d", 
								&(tri.idx0), &(tri.ndx0),  
								&(tri.idx1), &(tri.ndx1),
								&(tri.idx2), &(tri.ndx2)) == EOF) {
			std::cerr<<"Cannot parse this line: "<<line<<std::endl;
			return;
		}
	}
	// 4. f v1/t1/n1 v2/t2/n2 v3/t3/n3
	else if (cnt == 6) {		
		if (sscanf(line.c_str(),"f %d/%d/%d %d/%d/%d %d/%d/%d", 
								&(tri.idx0), &(tri.vdx0), &(tri.ndx0), 
								&(tri.idx1), &(tri.vdx1), &(tri.ndx1),
								&(tri.idx2), &(tri.vdx2), &(tri.ndx2)) == EOF) {
			std::cerr<<"Cannot parse this line: "<<line<<std::endl;
			return;
		}
	}

	// If successfully parsing the triangle data, add it to the vector
	// Decrement the index by 1
	tri.idx0--; tri.idx1--; tri.idx2--;
	tri.vdx0--; tri.vdx1--; tri.vdx2--;
	tri.ndx0--; tri.ndx1--; tri.ndx2--;	

	tris.push_back(tri);

}

void Mesh::normalize() {
	Point3f center(0.0, 0.0, 0.0);

	// Step 1: compute the center (average)	of the object
	for (unsigned int i = 0; i < verts.size(); i++) {
		center = center + verts[i].coord;
	}

	center = center * (1.0f / verts.size());

	// Step 2: shift the object to the center
	float scale = 0.f;

	for (unsigned int i = 0; i < verts.size(); i++) {
		verts[i].coord = verts[i].coord - center;
		// Compute the scaling factor
		if (fabs(verts[i].coord.x) > scale) scale = fabs(verts[i].coord.x);
		if (fabs(verts[i].coord.y) > scale) scale = fabs(verts[i].coord.y);
		if (fabs(verts[i].coord.z) > scale) scale = fabs(verts[i].coord.z);			
	}

	// Step 3: normalize the coordinates
	for (unsigned int i = 0; i < verts.size(); i++) {
		verts[i].coord = verts[i].coord * (1.0 / scale);		
	}	

}

/* Read boundary of the mesh from file */
void Mesh::initbound(const std::string& filename) {
	std::ifstream ifs(filename.c_str());
	std::string line;

	// Check whether the file is open 
	if (! ifs.is_open()) {
		std::cerr<<"Cannot open file "<<filename<<std::endl;
		return;
	}

	int idx1, idx2;

	// Start parsing .obj data
	while (std::getline(ifs, line)) {
		if (sscanf(line.c_str(),"%d %d", &(idx1), &(idx2)) == EOF) {
			std::cerr<<"Cannot parse this line: "<<line<<std::endl;
			return;
		}	

		/*if (verts[idx1].coord.x == -1 && verts[idx2].coord.x == -1) 
		    std::cout <<  idx1 << " "<< idx2 << std::endl;
		if (verts[idx1].coord.x == 1 && verts[idx2].coord.x == 1)
		    std::cout << idx1 << " " <<idx2 << std::endl;
		*/
		bound.push_back(LineSeg(verts[idx1].coord, verts[idx2].coord));
	}
}


/* Calculate the rectangular boundary for simplicity */
void Mesh::calcbound() {
	float xmin = 1e10;
	float xmax = -1e10;
	float ymin = 1e10;
	float ymax = -1e10;

	for (unsigned int i = 0; i < verts.size(); i++) {
		if (verts[i].coord.x < xmin) xmin = verts[i].coord.x;
		if (verts[i].coord.x > xmax) xmax = verts[i].coord.x;
		if (verts[i].coord.y < ymin) ymin = verts[i].coord.y;
		if (verts[i].coord.y > ymax) ymax = verts[i].coord.y;
	}

	bound.push_back(LineSeg(Point3f(xmin, ymin, 0.0), Point3f(xmax, ymin, 0.0)));
	bound.push_back(LineSeg(Point3f(xmax, ymin, 0.0), Point3f(xmax, ymax, 0.0)));	
	bound.push_back(LineSeg(Point3f(xmax, ymax, 0.0), Point3f(xmin, ymax, 0.0)));
	bound.push_back(LineSeg(Point3f(xmin, ymax, 0.0), Point3f(xmin, ymin, 0.0)));
	length = xmax - xmin;
	width  = ymax - ymin;
}

/* Test functions */
void Mesh::print_verts() {
	for (unsigned int i = 0; i < verts.size(); i++)
		std::cout<< verts[i].coord.x << " " << verts[i].coord.y << " " << verts[i].coord.z << std::endl;
}

void Mesh::print_tris() {
	for (unsigned int i = 0; i < tris.size(); i++)
		std::cout<< tris[i].idx0 << " " << tris[i].idx1 << " " << tris[i].idx2 << std::endl;
}

void Mesh::draw(void){
	if(!this->has_tex && !this->has_norm){
		drawV();
	} else if(this->has_tex && this->has_norm) {
		drawVNT();
	} else if(!this->has_tex && this->has_norm) {
		drawVN();
	} else if(this->has_tex && !this->has_norm) {
		drawVT();
	}
}

void Mesh::drawV(void){
	
}

void Mesh::drawVN(void){
	
}

void Mesh::drawVT(void){
	
}

void Mesh::drawVNT(void){
	
}	

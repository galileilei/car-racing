#include "lineseg.h"

bool LineSeg::isIntersect(LineSeg l) {
	float check1 = determinant(l.p1, p1, p2);
	float check2 = determinant(l.p2, p1, p2);
	float check3 = determinant(p1, l.p1, l.p2);
	float check4 = determinant(p2, l.p1, l.p2);

	if (check1 * check2 <=0 && check3 * check4 <= 0) return true;

	return false;
}
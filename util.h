#ifndef UTIL_H
#define UTIL_H

#include <GL/glut.h>
#include <math.h>
#include <iostream>
#include "SimpleImage.h"

// a 3D Point with coordinates (x, y, z)
struct Point3f {
    float x, y, z;

    Point3f() { x = 0. ;  y = 0. ; z = 0.; }
    Point3f(float _x, float _y, float _z) { x = _x; y = _y; z = _z;}

    Point3f operator*(float s) { return Point3f(x * s, y * s, z * s); }
    Point3f operator-() { return Point3f(-x, -y, -z); }
    Point3f operator-(Point3f const o) { return Point3f(x - o.x, y - o.y, z - o.z); }
    Point3f operator+(Point3f const o) { return Point3f(x + o.x, y + o.y, z + o.z);}
    Point3f operator/(float s) {
	if(s < 10e-15 && s > -10e-15 )
	    std::cout << "Warning :Divide by zero" <<std::endl;
    
	return Point3f(x / s, y / s, z / s);
    }

    // Norm
    float norm() { return sqrt(x*x + y*y + z*z); }
};

// Cross product
inline Point3f cross_product(Point3f a, Point3f b) {
    return Point3f(a.y*b.z - a.z*b.y,
		   a.z*b.x - a.x*b.z,
		   a.x*b.y - a.y*b.x);
}

// Orientation - just in the xy plane, the sign of the results indicates whether a,b,c are clockwise or counterclockwise
inline float determinant(Point3f a, Point3f b, Point3f c) {
    return (a.x * b.y + b.x * c.y + c.x * a.y - a.x * c.y - b.x * a.y - c.x * b.y);
}

// a 2D Point with coordinates (x, y)
struct Point2f {
    float x, y;

    Point2f() { x = 0. ;  y = 0. ; }
    Point2f(float _x, float _y) { x = _x; y = _y; }

    Point2f operator*(float s) { return Point2f(x * s, y * s); }
    Point2f operator-() { return Point2f(-x, -y); }
    Point2f operator-(Point2f const o) { return Point2f(x - o.x, y - o.y); }
    Point2f operator+(Point2f const o) { return Point2f(x + o.x, y + o.y); }
    
    float norm() { return sqrt(x*x + y*y); }

};

//rotating a vector on x-y plane counter clockwise
inline Point2f rotate_ccw(Point2f vec, float yaw){
	float xx = vec.x * cos(yaw) - vec.y * sin(yaw);
	float yy = vec.x * sin(yaw) + vec.y * cos(yaw);
	return Point2f(xx, yy);
}

inline Point2f rotate_cw(Point2f vec, float yaw){
	return rotate_ccw(vec, -yaw);
}

/* Helper function to take the screen shot */
inline void screenshot(const std::string& filename) {
    int w = glutGet(GLUT_WINDOW_WIDTH);
    int h = glutGet(GLUT_WINDOW_HEIGHT);
    float out[3 * w * h];
    RGBColor BG(0,0,0);
    SimpleImage shot(w, h, BG);
    glReadPixels(0, 0, w, h, GL_RGB, GL_FLOAT, &out[0]);
    for (int y = 0; y < h; ++y){
	for(int x = 0; x < w; ++x){
	    int index = (3 * w * y) + 3*x;
	    float red = out[index];
	    float green = out[index + 1];
	    float blue = out[index + 2];
	    shot.set(x, h - y,RGBColor(red, green, blue));
	}
    }
    shot.save(filename);
}

inline float sgn(float a){ return a > 0 ? 1.f : -1.f;}
#endif

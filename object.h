#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <vector>
#include <string>
#include "mesh.h"
#include <ctime>

struct Race {
    bool curve1;
    bool curve2;
    int circle;
};

struct Game_State {
    Point3f displacement;
    Point3f vel, acc;
    float vel_max;
    Point3f up, init_dir; // unit vector representing up and initial front facing direction
    float tick, dt, d_theta;
    clock_t last_time;
    float theta; // measure the angle between currect dirctiona and initial direction in radian; +ve in CCW and -ve in CW.
    float camera_height, camera_dist;
    float fovy, aspect, near, far;

    Race race;
   
};
    
class Object {
	public:
		// Give each object a name
		std::string name;
		
		// Mesh stores the current coordinates and connections
		Mesh mesh; 
	
		// Constructor
		Object(std::string name, Mesh& mesh);	

		// Transformations - scale and traslate
		void scale(float sx, float sy, float sz);
		void translate(float tx, float ty, float tz);
		//Point3f car_initial_position, car_initial_dir;
		float car_length;
		
};

#endif

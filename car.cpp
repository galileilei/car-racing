#include "car.h"
#include <algorithm>
#include <iostream>
/* implementing from the following paper:
http://nccastaff.bournemouth.ac.uk/jmacey/MastersProjects/MSc12/Srisuchat/Thesis.pdf
*/

Point2f Car::car2world(Point2f vec){
	float s = this->yaw;
	float x = vec.x * cos(s) + vec.y * (-sin(s));
	float y = vec.x * sin(s) + vec.y * cos(s);
	return Point2f(x, y);
}

Point2f Car::world2car(Point2f vec){
	float s = this->yaw;
	float x = vec.x * cos(s) + vec.y * sin(s);
	float y = vec.x * (-sin(s)) + vec.y * cos(s);
	return Point2f(x, y);
}

void Car::update(void){
	if(this->velocity.norm() > this->speed_limit){
		high_speed_update();
	} else {
		low_speed_update();
	}
}

void Car::high_speed_update(void){
	// get longitude and lateral speed from world coordinate
	this->vel = world2car(this->velocity);
	this->acc = world2car(this->acceleration);
	Point2f F = car2world(total_force());
	// update the position of COM of car
	this->acceleration = Point2f(F.x/this->mass, F.y/this->mass);
	this->velocity = this->velocity + this->acceleration * this->dt;
	this->pos = this->pos + this->velocity * this->dt;
	// update the rotation of drive wheel (rear wheel)
	this->rear_alpha = wheel_total_torque()/ this->wheel_inertia;
	this->rear_omega = this->rear_omega + this->rear_alpha * dt;
	// update the rpm of engine
	this->rpm = this->rear_omega * this->gearbox[this->gear_idx] * this->differential_ratio * 60/ (2 * M_PI);
	// TODO: apply auto-transmission
	// update the rotation of car w.r.t COM
	this->alpha = lateral_torque()/this->inertia;
	this->omega = this->omega + this->alpha * dt;
	this->yaw	= this->yaw + this->omega * dt;
}

void Car::low_speed_update(void){
	this->vel = world2car(this->velocity);
	this->acc = world2car(this->acceleration);
	float force = total_force().x;
	// update the position of COM of car
	float accel = force/this->mass;
	float veloc = this->vel.norm() + accel * dt;
	if(abs(this->sigma) >= 1e-4){
		// if turning, pos follow a circle
		float R = this->car_length / sin(this->sigma);
		// update rotation speed around a circle
		this->omega = this->velocity.norm() / R;
		float theta = this->omega * dt;
		// update car speed
		this->vel = rotate_ccw(Point2f(veloc, 0.), theta);
		this->velocity = car2world(this->vel);
		// update its yaw angle
		this->yaw = this->yaw + this->omega * dt;
		// update its new position
		Point2f radius = Point2f(0.f, R);
		Point2f dx = radius - rotate_ccw(radius, theta);
		this->pos = this->pos + dx;
	} else {
		this->vel = Point2f(veloc, 0.);
		this->velocity = car2world(this->vel);
		this->pos = this->pos + this->velocity * dt;
	} 
	// update the rotation of drive wheel (rear)
	this->rear_alpha = wheel_total_torque()/ this->wheel_inertia;
	this->rear_omega = this->rear_omega + this->rear_alpha * dt;
	// update the rotation of steering wheel (front)
	this->front_omega = this->velocity.norm()/(2 * M_PI * this->wheel_radius);
	// update rpm of engine
	this->rpm = this->rear_omega * this->gearbox[this->gear_idx] * this->differential_ratio * 60/ (2 * M_PI);
	if( this->rpm < this->min_rpm)
		this->rpm = this->min_rpm;
	if(this->rpm > this->max_rpm && this->gear_idx < 5){
		this->rpm = this->gearbox[this->gear_idx]/this->gearbox[this->gear_idx + 1];
		this->rpm = std::min(this->max_rpm, this->rpm);
		this->gear_idx++;
	}
}

/* simplified, piecewise linear torque curve */
float Car::max_engine_torque(float rpm){
	if(rpm >= this->min_rpm && rpm <= 1000.f) return 220.f;
	if(rpm > 1000.f && rpm <= 5000.f) return 220.f + 0.0225 * (rpm - 1000.f);
	if(rpm > 5000.f && rpm <= this->max_rpm) return 310.f - 0.05 * (rpm - 5000.f);
	return 0.f;
}

float Car::drive_force(float rpm){
	float e_tor = max_engine_torque(rpm) * this->throttle;
	std::cout << "rpm is " << rpm << ", engine torque is " << e_tor << std::endl;
	return e_tor * this->gearbox[this->gear_idx] * this->differential_ratio / this->wheel_radius;
}

float mod_atan(float numer, float denom){
	if(abs(numer) < 1e-8 && abs(denom) < 1e-8)
		return 0.f;
	return atan(numer/denom); 
}

float Car::slip_ratio(float r_wheel){
	float v = this->vel.x;
	//if ( abs(v) < 1e-8 ) return 0.f;
	if (abs(v) <= 1e-4)
		return (r_wheel * this->wheel_radius - v)/ (1e-4);
	else 
		return (r_wheel * this->wheel_radius - v)/ abs(v);
}

float Car::wheel_traction_force(float r_wheel, float load){
	float slip = slip_ratio(r_wheel);
	std::cout << "slip ratio = " << slip << std::endl;
	float ans = this->traction_constant * slip * load/this->normalised_load;
	float limit = this->max_friction_force * load/this->normalised_load;
	return std::max(-limit, std::min(limit, ans));
}

float Car::wheel_total_torque(void){
	//float w1 = this->front_omega;
	float w2 = this->rear_omega; //TODO: initially it is zero
	//float l1 = front_load();
	float l2 = rear_load();
	float R = this->wheel_radius;
	//float t1 = wheel_traction_force(w1, l1) * R;
	float t2 = wheel_traction_force(w2, l2) * R;
	float drive_torque = drive_force(this->rpm) * R;
	//TODO: add brake torque here too
	std::cout << "traction torque = " << t2 << ",drive torque = " << drive_torque << ", net torque = " <<  drive_torque - 2 * t2 << std::endl;
	// TODO: make sure it is always positive
	return drive_torque - 2 * t2;
}
//eqn 5.2
float Car::rear_slip_angle(void){
	Point2f V = this->vel;
	if(V.norm() < this->speed_limit) return 0.f;
	float w = this->omega;
	float c = this->distance_to_rear_axis;
	return mod_atan((V.y + w * c), abs(V.x));
}

//eqn 5.2
float Car::front_slip_angle(void){
	Point2f V = this->vel;
	float s = this->sigma;
	if(V.norm() < this->speed_limit) return -s * sgn(V.x);
	float w = this->omega;
	float b = this->distance_to_front_axis;
	return mod_atan((V.y + w * b), abs(V.x))- s * sgn(V.x);
}

//eqn 5.3
float Car::cornering_force(float angle, float load){
	float C_a = this->cornering_stiffness;
	float max_f = this->max_friction_force;
	return std::min(angle * C_a, max_f) * load /5000.f;
}

//eqn 5.4
float Car::lateral_torque(void){
	float a1 = front_slip_angle();
	float a2 = rear_slip_angle();
	float w1 = front_load(); //TODO: implement front and rear load
	float w2 = rear_load();
	
	float f1 = cornering_force(a1, w1);
	float f2 = cornering_force(a2, w2);
	
	float s = this->sigma;
	float b = this->distance_to_front_axis;
	float c = this->distance_to_rear_axis;
	return cos(s) * f1 * b - f2 * c;
}

// rolling resistance
Point2f Car::rolling_resistance(void){
	Point2f V = this->vel;
	float C = this->rolling_coef;
	return Point2f(-C * V.x, -C * V.y);
}

// drag assistance
Point2f Car::drag_resistance(void){
	Point2f V = this->vel;
	float C = this->drag_coef;
	return Point2f(-C * V.x * V.x, -C * V.y * V.y);
}

//
Point2f Car::total_force(void){
	Point2f tot_resistance = rolling_resistance() + drag_resistance();
	float f_tract = 2 * wheel_traction_force(this->rear_omega, rear_load());
	
	float a1 = front_slip_angle();
	float a2 = rear_slip_angle();
	float w1 = front_load();
	float w2 = rear_load();
	float f_front = cornering_force(a1, w1);
	float f_rear = cornering_force(a2, w2);
	
	float s = this->sigma;
	float f_x = f_tract + f_front * sin(s) + tot_resistance.x;
	float f_y = f_rear + f_front * cos(s) + tot_resistance.y;
	return Point2f(f_x, f_y);
}

float Car::front_load(void){
	float a = this->acc.x;
	//float b = this->distance_to_front_axis;
	float c = this->distance_to_rear_axis;
	float L = this->car_length;
	float M = this->mass;
	float W = this->mass * this->G;
	float h = this->height_to_COM;
	
	return (c/L) * W - (h/L) * M * a;
}

float Car::rear_load(void){
	float a = this->acc.x;
	float b = this->distance_to_front_axis;
	//float c = this->distance_to_rear_axis;
	float L = this->car_length;
	float M = this->mass;
	float W = this->mass * this->G;
	float h = this->height_to_COM;
	
	return (b/L) * W + (h/L) * M * a;
}


void Car::update_rpm_from_velocity(void){
/*
	//assume it is rolling
	float V = this->velocity.norm();
	float r_wheel = V/this->wheel_radius;
	float ans = r_wheel * this->gearbox[this->gear_idx] * this->differential_ratio * 60 / (2 * M_PI);
	this->rpm = ans;
*/
}

void Car::auto_transmission(void){
/*
	float v = this->vel.x;
	if(this->gear_idx < 5){
		float v_max = max_possible_rpm(this->gear_idx + 1);
		if(v > v_max) {
			this->gear_idx++;
			return;
		}
	}
	if(this->gear_idx == 0)
		return;
	if(this->gear_idx > 0){
		float v_min = max_possible_rpm(this->gear_idx - 1);
		if( v < v_min){
			this->gear_idx--;
			return;
		}
	}
*/
}

void Car::status(void){
	//out put control
	std::cout << "steering = " << this->sigma / M_PI * 180.f << ", throttle = " << this->throttle << std::endl;
	// output various parameters
	std::cout << "dt = " << this->dt << "s, rpm = " << this->rpm << ", pos = (" << this->pos.x << ", " << this->pos.y << "), vel =  (" << this->velocity.x << ", " << this->velocity.y << "), omega = (" << this->front_omega << ", " << this->rear_omega << ")." << std::endl;
}

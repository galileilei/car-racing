#include <fstream>
#include <iostream>
#include <string>
#include <math.h>
#include <iomanip>
using namespace std;

int main() {
	string filename = "cube/tex-cube.obj";
	string line;
	ifstream ifs(filename.c_str());
	ofstream new_file;
	new_file.open("cube/envir-cube.obj");

		if (! ifs.is_open()) {
		std::cerr<<"Cannot open file "<<filename<<std::endl;
		return 0;
	}

	// Start parsing .obj data
	while (std::getline(ifs, line)) {
	
		if (line.substr(0, 2) == "v ") {
			float x, y, z;
			if (sscanf(line.c_str(),"v %f %f %f", &x, &y, &z) == EOF) {
				std::cerr<<"Cannot parse this line: "<<line<<std::endl;
				return 0 ;
			}
			new_file <<setprecision(8)<< "v  " << z  - 0.5 <<" " << x - 0.5 << " " << 1 - y - 0.00001 << std::endl;
			
			
		}

		else
			new_file << line << endl;
	
	}

	return 0;
}

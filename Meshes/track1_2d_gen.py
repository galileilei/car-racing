# Input to read
infile = open("track1_2d.mphtxt", "r");
outfile = open("track1_2d.obj", "w");
lineNum = 0;

for line in infile:
	lineNum = lineNum + 1;
	if line == "": 
		continue;
	if lineNum <= 149:
		line = line.rstrip("\r\n");
		outfile.write("v " + line + "0.0\n");
	else:
		line = line.rstrip("\r\n");
		line = line.split(' ');
		outfile.write("f " + str(int(line[0])+1) + " " + str(int(line[1])+1) + " " + str(int(line[2])+1) + " \n");


# Input to read
infile = open("track1.mphtxt", "r");
outfile = open("track1.obj", "w");
lineNum = 0;

for line in infile:
	lineNum = lineNum + 1;
	if line == "": 
		continue;
	if lineNum <= 333:
		outfile.write("v " + line);
	else:
		line = line.rstrip("\r\n");
		line = line.split(' ');
		outfile.write("f " + str(int(line[0])+1) + " " + str(int(line[1])+1) + " " + str(int(line[2])+1) + " \n");


#include "math.h"
#include "util.h"

/* turning changes steering angle, sigma
 * accelation changes rpm of engine, rpm
 * braking applies a contant braking force
 */

struct Car{
	public:
	Point2f pos;
	float yaw;
	float dt;
	Point2f acceleration, velocity; //vector.x and y stores value in world coordinate
	// player control
	float sigma;				// steering angle in radian;
	float throttle;				// jia you!!!
	void low_speed_update(void);
	void high_speed_update(void);
	void update(void);
	
	//private:
	Point2f acc, vel; 			// vector.x and y stores longtitude and lateral components respectively
	float omega, alpha; 		// angular component of car w.r.t Center of mass
	float rpm;					// engine rpm
	float rear_omega, rear_alpha;	//
	float front_omega, front_alpha; //	
	//float throttle; 			// a number between [0, 1]
	float front_slip_angle(void);
	float rear_slip_angle(void);
	float lateral_torque(void);
	float max_engine_torque(float rpm);
	float drive_torque(float rpm);
	void transform_ccordinate(void);
	float cornering_force(float angle, float load);
	Point2f drag_resistance(void);
	Point2f rolling_resistance(void);
	float front_load(void);
	float rear_load(void);
	void auto_transmission(void);
	Point2f total_force(void);
	Point2f world2car(Point2f vec);
	Point2f car2world(Point2f vec);
	void update_rpm_from_velocity(void);
	float max_possible_rpm(int idx);
	float wheel_total_torque(void);
	float drive_force(float rpm);
	float wheel_traction_force(float r_wheel, float load);
	float slip_ratio(float r_wheel);
	void status(void);
	int gear_idx;
	///////////////////////////////////
	/* Various Constant              */
	///////////////////////////////////
	float G = 9.81f;					//9.81 m s^-2
	float mass = 1500.f;				//1500 kg
	float inertia = 3625.f;				// assuming width 2m
	float car_length = 5.f;				//5 m
	float distance_to_front_axis = 1.f; //1 m
	float distance_to_rear_axis = 4.f;	//4 m
	float height_to_COM = 0.5f;			//.5 m
	float wheel_radius = 0.33f;			//13 inches = 33.3cm
	float cornering_stiffness = 5000.f; // 5000 at 5kN
	float max_friction_force = 6000.f; // at 5kN
	float gearbox[6] = {2.97, 2.07, 1.43, 1.00, 0.84, 0.56}; // Chevrolet Corvette C5 Z06 manual transmission, final drive ratio included
	float differential_ratio = 3.42;
	float rolling_coef = 12.8f;
	float drag_coef = 0.4257f;
	float min_rpm = 200.f;
	float max_rpm = 8000.f;
	float max_sigma = 60.f * M_PI/180.f;
	float wheel_inertia = 10.f;		//75 kg wheel with 33 cm radius
	float traction_constant = 1000.f;   // cap at 6% slip ratio
	float normalised_load = 5000.f;		// standard load
	float speed_limit = 20.f;			// 45 mph = 72 kmh = 20ms^-1
};

#include <iostream>
#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "math.h"

#include "car.h"
#include "object.h"

#define TOL 1e-4


// Global variable for mesh
Mesh mesh;

// Global variable for objects
enum obj{ENV = 0, TRACK, CAR};
std::vector<Object> objects;

// Global variables for mouse state and rotation angles

Game_State game_state;
Car car;

bool leftMouseDown = false;
bool middleMouseDown = false;
 
float x_angle = 0.0f;
float y_angle = 0.0f;
 
float x_diff = 0.0f;
float y_diff = 0.0f;

float global_scale = 1.0;
int   y_orig = 0;

std::string outputFileName;
//static GLuint texName, texName2, tileName;
static GLuint texName[3];
//Use to debug

double mod(double numer, double denom) {
    double ans = fmod(numer, denom);
    return ans < 0 ? denom - ans: ans;
}

void SetupOpenGLParams(void);

void debug(void) {
    std::cout << "displacement"<< std::endl;
    std::cout << game_state.displacement.x <<" "<< game_state.displacement.y<<" "<< game_state.displacement.z << std::endl;
    std::cout <<"velocity" << std::endl; 
    std::cout << game_state.vel.x <<" "<< game_state.vel.y<<" "<< game_state.vel.z << std::endl;
    std::cout <<"acceleration" << std::endl; 
    std::cout << game_state.acc.x <<" "<< game_state.acc.y<<" "<< game_state.acc.z << std::endl;
   
    
}

Point3f steering_direction(void){
    Point3f dir = game_state.init_dir;
    //float theta = car.sigma;
    float theta = game_state.theta;
	
    Point3f up = game_state.up;
    /* game_state.vel  = game_state.vel*cos(d_theta) - cross_product(game_state.vel, game_state.up)*sin(d_theta); */
    Point3f right = cross_product(dir, up);
    return dir * cos(theta) - right * sin(theta);
}

// Set up a camera behind car
void carview(void){
	// set up camera viewing angle
	glLoadIdentity();
    gluPerspective(game_state.fovy, game_state.aspect, game_state.near, game_state.far);
    // set up camera position and direction
    Point3f center = game_state.displacement;
    Point3f front = steering_direction();
    
    Point3f eye = game_state.displacement - front*game_state.camera_dist + Point3f(0.0f,0.0f,game_state.camera_height);
    Point3f camera_up = game_state.up + front/2;
    camera_up = camera_up/camera_up.norm();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(	eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		camera_up.x,camera_up.y,camera_up.z);
}


void collision(){
    bool isColl = false;
    Point3f displacement = game_state.displacement + game_state.vel*game_state.dt;
    Point3f vel = game_state.vel;
    Point3f norm;
    //std::cout <<  "Collision Check bound size is " << objects[0].mesh.bound.size() << std::endl;
    // Detect collision
    for (unsigned i = 0; i < objects[1].mesh.bound.size(); i++) {
        if (isColl) break;
        for (unsigned j = 0; j < objects[0].mesh.bound.size(); j++) {
	    
            LineSeg carBound = LineSeg(objects[1].mesh.bound[i].p1 + displacement,
                                       objects[1].mesh.bound[i].p2 + displacement);

            if (carBound.isIntersect(objects[0].mesh.bound[j])) {
                // Test          
                isColl = true;
		norm = objects[0].mesh.bound[j].p1 - objects[0].mesh.bound[j].p2; 
                norm = Point3f(norm.y, -norm.x, 0.0);
		norm = norm/norm.norm();
		break;
            }
        }
    }

    // If collide, change the velocity 
    if (isColl){
	std::cout <<" Collision " << std::endl;
	std::cout << "norm is " <<  norm.x <<" "<<norm.y << " "<< norm.z <<std::endl;

	game_state.displacement = game_state.displacement - norm*(vel.x*norm.x + vel.y*norm.y)* 0.5* game_state.dt; 
	game_state.vel = vel - norm*(vel.x*norm.x + vel.y*norm.y)*0.5;
       
	game_state.vel = game_state.vel/4.0;
    }

    
    
}

void update_race(void) {
    if(game_state.displacement.x > 1.0f)
	game_state.race.curve1 = true;
    if(game_state.race.curve1 && game_state.displacement.x < -1.0)
	game_state.race.curve2 = true;
    if(game_state.race.curve1 && game_state.race.curve2 && game_state.displacement.x > -1.0 &&
       game_state.displacement.y > 0.5){
	game_state.race.circle ++;
	game_state.race.curve1 = false;
	game_state.race.curve2 = false;
    }
	
	
	    
}
void drawspeed(float speed) {
    char s[30];
    sprintf(s, "Speed: %f",speed);
    glRasterPos2i(15, 8);
    char* c;
    for(c = s; *c != '\0'; c++)
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
}

void drawtime(float time) {
    char s[30];
    sprintf(s, "Time: %f",time);
    glRasterPos2f(0.0f, 0.5f);
    char* c;
    for(c = s; *c != '\0'; c++)
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
}

void drawcollision() {
    char s[30];
    sprintf(s, "COLLISION!!");
    glRasterPos2f(0.5f, 0.5f);
    char* c;
    for(c = s; *c != '\0'; c++)
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *c);
}

void drawcircle() {
    char s[30];
    sprintf(s, "%d", game_state.race.circle);
    glRasterPos2f(0.0f, 0.5f);
    char* c;
    for(c = s; *c != '\0'; c++)
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *c);
}
void display(void) {
	//debug();
	//car.status();
    /* clear all pixels */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SetupOpenGLParams();
    carview();
	///////////////////////////////////////////
	/* various options enabled               */
	///////////////////////////////////////////
    update_race();
    //debug();
    //drawcircle();
    drawspeed(game_state.vel.norm());
    /* clear all pixels */

   
    /* PolygonMode for lines */
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /* enable culling */

    /* Set the color   */
    glColor3f(140.0/255.0, 21.0/255.0, 21.0/255.0);
     
    /* Scaling*/   
    // glScalef(global_scale, global_scale, global_scale);

    /* Rotations */
    //glRotatef(x_angle, 1.0f, 0.0f, 0.0f);
    //glRotatef(y_angle, 0.0f, 1.0f, 0.0f);  
	
	
    ///////////////////////////////////////////
    /* various options enabled               */
    ///////////////////////////////////////////
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    /* Draw each object and each triangle of the object */
    ////////////////////////////////////////////
    // Draw environment
    ////////////////////////////////////////////
    int i = 2;
    glBindTexture(GL_TEXTURE_2D, texName[ENV]);
    
    glBegin(GL_TRIANGLES);  
    for (unsigned int j = 0; j < objects[i].mesh.tris.size(); j++) {	  
	unsigned int idx0 = objects[i].mesh.tris[j].idx0;
	  
	unsigned int idx1 = objects[i].mesh.tris[j].idx1;
	unsigned int idx2 = objects[i].mesh.tris[j].idx2;

	unsigned int vdx0 = objects[i].mesh.tris[j].vdx0;
	  
	unsigned int vdx1 = objects[i].mesh.tris[j].vdx1;
	unsigned int vdx2 = objects[i].mesh.tris[j].vdx2;
	  
   

	glTexCoord2f(objects[i].mesh.texs[vdx0].x,1.0 - objects[i].mesh.texs[vdx0].y);
	  
	glVertex3f(objects[i].mesh.verts[idx0].coord.x,
		   objects[i].mesh.verts[idx0].coord.y, 
		   objects[i].mesh.verts[idx0].coord.z);
	    
	glTexCoord2f(objects[i].mesh.texs[vdx1].x,1.0 - objects[i].mesh.texs[vdx1].y);
	glVertex3f(objects[i].mesh.verts[idx1].coord.x,
		   objects[i].mesh.verts[idx1].coord.y, 
		   objects[i].mesh.verts[idx1].coord.z);

	glTexCoord2f(objects[i].mesh.texs[vdx2].x,1.0 - objects[i].mesh.texs[vdx2].y);
	glVertex3f(objects[i].mesh.verts[idx2].coord.x,
		   objects[i].mesh.verts[idx2].coord.y, 
		   objects[i].mesh.verts[idx2].coord.z);                 
     
    }
    glEnd();

    //////////////////////////
    // Draw track
    //////////////////////////
    i = 0;
    glBindTexture(GL_TEXTURE_2D, texName[TRACK]);
    
    glBegin(GL_TRIANGLES);  
    for (unsigned int j = 0; j < objects[i].mesh.tris.size(); j++) {
	unsigned int idx0 = objects[i].mesh.tris[j].idx0;
	unsigned int idx1 = objects[i].mesh.tris[j].idx1;
	unsigned int idx2 = objects[i].mesh.tris[j].idx2;
	float x0 = objects[i].mesh.verts[idx0].coord.x;
	float x1 = objects[i].mesh.verts[idx1].coord.x;
	float x2 = objects[i].mesh.verts[idx2].coord.x;
	float y0 = objects[i].mesh.verts[idx0].coord.y;
	float y1 = objects[i].mesh.verts[idx1].coord.y;
	float y2 = objects[i].mesh.verts[idx2].coord.y;     
	
	//std::cout << mod(x0, 1) << " " << mod(y0, 1) << std::endl;
	glTexCoord2f(x0, y0);
	glVertex3f(objects[i].mesh.verts[idx0].coord.x,
		   objects[i].mesh.verts[idx0].coord.y, 
		   objects[i].mesh.verts[idx0].coord.z);
	glTexCoord2f(x1, y1);
	glVertex3f(objects[i].mesh.verts[idx1].coord.x,
		   objects[i].mesh.verts[idx1].coord.y, 
		   objects[i].mesh.verts[idx1].coord.z);
	glTexCoord2f(x2, y2);
	glVertex3f(objects[i].mesh.verts[idx2].coord.x,
		   objects[i].mesh.verts[idx2].coord.y, 
		   objects[i].mesh.verts[idx2].coord.z);

    }
    glEnd();
	
    //////////////////////////
    // Draw car
    //////////////////////////
    std::cout << car.pos.x << " " << car.pos.y << " " << car.yaw << std::endl;
    i = 1;
    //glDisable(GL_CULL_FACE);
    glBindTexture(GL_TEXTURE_2D, texName[CAR]);
    
    Point3f up = game_state.up;     
    Point3f displacement = game_state.displacement;
    glTranslatef(displacement.x, displacement.y,displacement.z);
    float angle = car.yaw;
    glRotatef((angle)*180./M_PI, up.x, up.y, up.z);
    
    glBegin(GL_TRIANGLES);  
    for (unsigned int j = 0; j < objects[i].mesh.tris.size(); j++) {
	  
	unsigned int idx0 = objects[i].mesh.tris[j].idx0;
	  
	unsigned int idx1 = objects[i].mesh.tris[j].idx1;
	unsigned int idx2 = objects[i].mesh.tris[j].idx2;

	unsigned int vdx0 = objects[i].mesh.tris[j].vdx0;
	  
	unsigned int vdx1 = objects[i].mesh.tris[j].vdx1;
	unsigned int vdx2 = objects[i].mesh.tris[j].vdx2;
	
	glTexCoord2f(objects[i].mesh.texs[vdx0].x,1.0 - objects[i].mesh.texs[vdx0].y);
	  
	glVertex3f(objects[i].mesh.verts[idx0].coord.x,
		   objects[i].mesh.verts[idx0].coord.y, 
		   objects[i].mesh.verts[idx0].coord.z);
	    
	glTexCoord2f(objects[i].mesh.texs[vdx1].x,1.0 - objects[i].mesh.texs[vdx1].y);
	glVertex3f(objects[i].mesh.verts[idx1].coord.x,
		   objects[i].mesh.verts[idx1].coord.y, 
		   objects[i].mesh.verts[idx1].coord.z);

	glTexCoord2f(objects[i].mesh.texs[vdx2].x,1.0 - objects[i].mesh.texs[vdx2].y);
	glVertex3f(objects[i].mesh.verts[idx2].coord.x,
		   objects[i].mesh.verts[idx2].coord.y, 
		   objects[i].mesh.verts[idx2].coord.z);                 
     
    }
    glEnd();
    
    glDisable(GL_TEXTURE_2D);
    //glDisable(GL_CULL_FACE);

    /* Flush and swap */
    glFlush();
    glutSwapBuffers();
}

void SetupGameState(void) {
    /* select clearing (background) color */
    glClearColor(1.0, 1.0, 1.0, 0.0);
    /* initialize viewing values */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    /*initialize the game parameters*/
    //game_state.dt = 0.1;
    game_state.d_theta = 0.1;
    game_state.init_dir = Point3f(1.0f, 0.0f,0.0f);
    game_state.theta = 0.f;
    game_state.acc =  game_state.init_dir/game_state.init_dir.norm();
    game_state.last_time = std::clock();
    
    /*Initial the position of the car*/
    game_state.displacement= Point3f(-1.0, 0.625, 0.0);
    game_state.vel= Point3f(0.0,0.0,0.0);
    game_state.up= Point3f(0.0,0.0,1.0);  

    float car_length = objects[1].mesh.length;
    game_state.near = 0.1*car_length;
    std::cout << car_length << std::endl;
    game_state.far = 100.0;
    game_state.fovy = 90.0;
    game_state.aspect = 1.0;;
   
    game_state.camera_height = 0.5*car_length;
    game_state.camera_dist = 1.5*car_length;
    game_state.vel_max = car_length/ (4*game_state.dt);
    
    /* Intialize car position */
    
    car.pos = Point2f(-1.0, 0.625);
    car.acceleration = Point2f(0.f, 0.f);
    car.velocity = Point2f(0.f, 0.f);
    car.throttle = 0.f;
    car.yaw = 0.f;
    car.sigma = 0.f; 
    car.dt = game_state.dt;
    car.gear_idx = 0;
    car.front_omega = 0.f;
    car.rear_omega = 0.f;
    car.rpm = 400.f;
}

void SetupTexture(void) {
    glGenTextures(3, texName);
    int w, h;
	
    // set up enviroment cube
    glBindTexture(GL_TEXTURE_2D, texName[ENV]);
    SimpleImage cubePNG("Meshes/cube/cube2.jpg");
    w = cubePNG.width();
    h = cubePNG.height();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);	
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_FLOAT, cubePNG.data());
    
    // set up track
    glBindTexture(GL_TEXTURE_2D, texName[TRACK]);
    SimpleImage trackPNG("Meshes/tiles/aspsalt_small.jpeg");
    w = trackPNG.width();
    h = trackPNG.height();	

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);	
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_FLOAT, trackPNG.data());
    
    // set up car
    glBindTexture(GL_TEXTURE_2D, texName[CAR]);
    SimpleImage carPNG("Meshes/car1/car.bmp");
    w = carPNG.width();
    h = carPNG.height();	

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);	
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_FLOAT, carPNG.data());
}

void SetupOpenGLParams(void){
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
}

// This function is used for the navigation keys
void keyboard (unsigned char key, int x, int y)
{
    float d_theta = game_state.d_theta;
    float dt     = game_state.dt;
    Point3f acc    = game_state.acc;

    switch (key) {  
    
        // use key 'p' to take screen shot
    case 'p':               
	screenshot(outputFileName);
	break;

    case 'w':
    // updating car object
    if(car.rpm < car.min_rpm)
    	car.rpm = car.min_rpm + 1.f;
	car.throttle += 0.2f;
	car.throttle = std::min(1.f, car.throttle); 
	// updating game_state
	if (game_state.vel.norm() < game_state.vel_max)
	    game_state.vel = game_state.vel + acc*dt;
	collision();
	game_state.displacement = game_state.displacement + game_state.vel*dt;

	break;
	
    case 's':
	// updating car object
	car.throttle -= 0.2f;
	// updating game_state
	if (game_state.vel.norm() < game_state.vel_max)
	    game_state.vel = game_state.vel - acc*dt;
	collision();
	game_state.displacement = game_state.displacement + game_state.vel*dt;
	break;
	
    case 'a':
    car.sigma += 3.f * (M_PI / 180.f);
    car.sigma = std::min(car.max_sigma, car.sigma);
	break;  
	  
    case 'd':
	car.sigma -= 3.f * (M_PI / 180.f);
	car.sigma = std::max(-car.max_sigma, car.sigma);
	break;
    }
    clock_t time_now = std::clock();
    car.dt = float(time_now - game_state.last_time)/ CLOCKS_PER_SEC;
    game_state.dt = car.dt;
    game_state.last_time = time_now;
    car.update();
    game_state.displacement.x = (car.pos.x  + 1.f)/car.car_length * objects[1].mesh.length;
    game_state.displacement.y = (car.pos.y - 0.65)/car.car_length * objects[1].mesh.length;
    game_state.theta = car.yaw;
    //std::cout << car.pos.x << " " << car.pos.y << " " << car.yaw << std::endl;
    glutPostRedisplay();    // Redraw the scene
}

/* TODO: Update the car position / velocity when there is no input from the keybaord 
   We need to keep track the time and update the position correspondingly
   If the car has no velocity and acceleration, then do nothing in this function
*/
void idle() {
    // car will slow down due to friction, and throttle will drop and steering wheel will return to normal
    // first order approximation to exponential decay
    clock_t time_now = std::clock();
    car.dt = float(time_now - game_state.last_time)/ CLOCKS_PER_SEC;
    game_state.dt = car.dt;
    game_state.last_time = time_now;
    car.throttle *= (1 - 0.95 * car.dt);
    car.sigma *= ( 1 - 0.95 * car.dt);
    car.update();
	game_state.displacement.x = car.pos.x;
    game_state.displacement.y = car.pos.y;
    game_state.theta = car.yaw;
    //collision();
    // move the car...
    //game_state.vel = game_state.vel*0.999;
    //game_state.displacement = game_state.displacement + game_state.vel*game_state.dt;

    collision();
    glutPostRedisplay(); 
}

/* TODO: Collision detection and update the car movement correspondingly */

int main(int argc, char** argv) {
   
    // Usage of the program  
    if (argc < 3) {
	std::cout<<"Usage ./main2 mesh-file-path screeshot-file-name"<<std::endl;
	return 0;
    }

    // Input the mesh
    std::string meshFileName = argv[1];
    outputFileName = argv[2];

    mesh.initialize(meshFileName);
    mesh.initbound("Meshes/track1_2d_bound.txt");

    // Setup the objects
    Object obj = Object("track", mesh);
    //obj.scale(5.0, 10.0, 1.0);
    objects.push_back(obj);

    // Set up the car object
    Mesh mesh2;
    mesh2.initialize("Meshes/car1/bluecar.obj");
    Object obj2 = Object("car", mesh2);
    obj2.scale(0.0003, 0.0003, 0.0003);
    obj2.mesh.calcbound();
    objects.push_back(obj2);

    Mesh mesh3;
    mesh3.initialize("Meshes/cube/envir-cube.obj");
    Object obj3 = Object("enviroment",mesh3);
    obj3.scale(5.0,5.0,5.0);
    objects.push_back(obj3);
	


    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Mesh Wireframe Display");
    SetupGameState();
	SetupTexture();
	SetupOpenGLParams();
    
    glutDisplayFunc(display);
    //glutMouseFunc(mouse);
    //glutMotionFunc(mouseMotion);   
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
    glutMainLoop();   


    return 0;   /* ANSI C requires main to return int. */
}

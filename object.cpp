#include <GL/glut.h>
#include "object.h"
#include "math.h"

Object::Object(std::string name, Mesh& mesh) {
	this->name = name;
	this->mesh = mesh;
	//this->vel = Point3f(0, 0, 0);
	//this->acc = Point3f(0, 0, 0);

	for (unsigned int i = 0; i < this->mesh.tris.size(); i++) {
		if (this->mesh.verts[this->mesh.tris[i].idx0].coord.z <= 1e-5 &&
			this->mesh.verts[this->mesh.tris[i].idx1].coord.z <= 1e-5 &&
			this->mesh.verts[this->mesh.tris[i].idx2].coord.z <= 1e-5) {
			this->mesh.tris[i].color = RGBColor(140.0/255.0, 21.0/255.0, 21.0/255.0);
		} else {
			this->mesh.tris[i].color = RGBColor(1, 1, 1);
		}	
	}

	// Normalize the mesh
	// mesh.normalize();
}

// Scale the object
void Object::scale(float sx, float sy, float sz) {
	for (unsigned int i = 0; i < this->mesh.verts.size(); i++) {
		this->mesh.verts[i].coord.x *= sx;
		this->mesh.verts[i].coord.y *= sy;
		this->mesh.verts[i].coord.z *= sz;
	}	
}

void Object::translate(float tx, float ty, float tz) {
	for (unsigned int i = 0; i < this->mesh.verts.size(); i++) {
		this->mesh.verts[i].coord.x += tx;
		this->mesh.verts[i].coord.y += ty;
		this->mesh.verts[i].coord.z += tz;
	}	
}
/*
void Object::blue_car_init(void) {
    	for (unsigned int i = 0; i < this->mesh.verts.size(); i++) {
	    
	    float x = this->mesh.verts[i].coord.x;
	    float y = this->mesh.verts[i].coord.y;
	    float z = this->mesh.verts[i].coord.z;
    
	    this->mesh.verts[i].coord.x = z;
	    this->mesh.verts[i].coord.y = x;
	    this->mesh.verts[i].coord.z = y;
	}
}   
*/

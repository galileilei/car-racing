CC=g++
CFLAGS=-Wall -g -std=gnu++11
LIBS = -lGL -lGLU -lglut
PREC = SimpleImage stb mesh object lineseg car
OBJSUFFIX = .o
TESTFILE = test

OBJS =  $(addsuffix $(OBJSUFFIX), $(PREC))

%.o : %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

mesh.o : SimpleImage.o stb.o
	$(CC) -c -o mesh.o mesh.cpp $(CFLAGS) $(LIBS)

all: carrace

carrace: $(OBJS)
	$(CC) -o carrace main.cpp $(OBJS) $(CFLAGS) $(LIBS)

test: car.o
	$(CC) -o $(TESTFILE) test.cpp car.o $(CFLAGS) $(LIBS)

clean:
	rm carrace *.o
